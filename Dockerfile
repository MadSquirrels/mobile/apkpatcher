FROM python:3.12

# Install Java
RUN apt update && apt upgrade -y && apt-get install openjdk-17-jdk -y \
      && apt-get purge -y \
      && apt-get clean \
      && rm -rf /var/lib/apt/lists/* /var/cache/apt/archives /tmp/* /var/tmp/*

# Install apkpatcher
COPY . /work
RUN cd /work && pip install .
RUN rm -rf /work && apkpatcher --download-jars

# Create a user
RUN useradd -m -u 1000 user
USER user

# Get the SDK tools 
WORKDIR /sdktools
RUN wget https://dl.google.com/android/repository/commandlinetools-linux-6200805_latest.zip \
  && unzip commandlinetools-linux-6200805_latest.zip \
  && mkdir cmdline-tools \
  && mv tools/ cmdline-tools/

ENV ANDROID_SDK_ROOT /sdktools

# Install build tools
RUN /sdktools/cmdline-tools/tools/bin/sdkmanager --update \
  && yes | /sdktools/cmdline-tools/tools/bin/sdkmanager --licenses \
  && /sdktools/cmdline-tools/tools/bin/sdkmanager "platform-tools" "platforms;android-35" "build-tools;36.0.0-rc5"

WORKDIR /pwd


ENTRYPOINT ["python", "-m", "apkpatcher"] 
