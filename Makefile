default: help

help: # Show help for each of the Makefile recipes.
	@grep -E '^[a-zA-Z0-9 -]+:.*#'  Makefile | sort | while read -r l; do printf "\033[1;32m$$(echo $$l | cut -f 1 -d':')\033[00m:$$(echo $$l | cut -f 2- -d'#')\n"; done

install:
	uv build
	pip install dist/*.whl

install-dev:
	uv pip install .

lint: # Format with black and lint with ruff.
	@echo "[+] Linting"
	uv run black -l 100 src

type-check: # Run mypy.
	@echo "[+] Type checking"
	uv run mypy --config-file pyproject.toml src || true
	uv run pylint --load-plugins=pylint_protobuf --disable=R0912,W0613,C0114,R0903,W0511,C0209,R0913,R1702,R0914,R0915,R0911,R0917,C0301,E1101,E0602,W0212,C0121 src

test: # Run pytest.
	@echo "[+] Run tests"
	uv run pytest . || true
