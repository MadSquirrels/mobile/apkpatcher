apkpatcher package
==================

Submodules
----------

apkpatcher.cli module
---------------------

.. automodule:: apkpatcher.cli
   :members:
   :undoc-members:
   :show-inheritance:

apkpatcher.conf module
----------------------

.. automodule:: apkpatcher.conf
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: apkpatcher
   :members:
   :undoc-members:
   :show-inheritance:
