.. APKPATCHER documentation master file, created by
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to APKpatcher's documentation |version| |release|!
==========================================================

APK Patcher is a Python utility designed to easily modify Android applications
(APK files). It can be used either as a standalone tool or as a Python library
integrated into other projects.

The primary goal of APK Patcher is to enable users to modify the Smali code of
an application. This allows for advanced tasks such as:

- Injecting libraries, like Frida, to facilitate debugging and analysis without
  the need for a rooted device.
- Unlocking hidden or experimental features that are not readily accessible in
  the original application.
- Adding or removing specific features, such as permissions, activities,
  services, or other components of the application.

Whether you're looking to customize an APK for testing, security research,
or personal use, APK Patcher offers a flexible, easy-to-use solution for
modifying Android apps.

The link of the project https://gitlab.com/MadSquirrels/mobile/apkpatcher

How to
======

.. toctree::
  :maxdepth: 2
  :titlesonly:
  :glob:

  how

Details
=======

.. toctree::
  :maxdepth: 2
  :titlesonly:
  :glob:

  modules



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
