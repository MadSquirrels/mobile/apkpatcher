import os
import unittest

import tempfile
import shutil
from pathlib import Path
from apkpatcher import Patcher


test_dir = os.path.dirname(os.path.abspath(__file__))



class AXMLTest(unittest.TestCase):
    """Unittest for apkpatcher

    """

    def testget_good_entrypoint(self):
        testscase = [
          ("/data/test_activity_alias.xml", "com.apkpatcher.pyaxml.ui.Activities.WelcomeActivity"),
          ("/data/test_multi_launcher.xml", "com.apkpatcher.pyaxml.SplashActivity")
        ]
        patcher = Patcher("app_didnt_exist.apk", "34.0.0", "/usr/lib/android-sdk")
        with tempfile.TemporaryDirectory() as tmp_dir:
            patcher.final_dir = Path(f"{tmp_dir}")
            for testfile, result in testscase:
                shutil.copyfile(test_dir + testfile, patcher.final_dir / "AndroidManifest.xml")
                self.assertEqual(patcher.get_entrypoint_class_name(), result)



if __name__ == '__main__':
    unittest.main()
