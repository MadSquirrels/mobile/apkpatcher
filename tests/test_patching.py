import os
import unittest
from apkpatcher.smalipatching import replace_methods, Method

test_dir = os.path.dirname(os.path.abspath(__file__))



class AXMLTest(unittest.TestCase):

    def test_patch_method(self):
        testscase = [
            ("/data/GradientColor.smali", ".method public setEndColor(I)V", "  .locals 0\n  .line 50\n    return-void", "/data/GradientColor_patched.smali")
        ]
        for fname, prototype, patch, verifier in testscase:
            m = Method(prototype, patch)
            with open(f"{test_dir}{fname}") as f: 
                content = f.read()
                text = replace_methods([m], content)
                with open(f"{test_dir}{verifier}") as f2:
                    self.assertMultiLineEqual(text, f2.read())




if __name__ == '__main__':
    unittest.main()
